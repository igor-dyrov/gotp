package main

import (
	"os"
	"bytes"
	"strings"
	"io/ioutil"
	"log"
	"strconv"
	"fmt"
)

const MIDDLEUNIT = "\n├───"
const LASTUNIT = "\n└───"

func removeFiles(objects []os.FileInfo) []os.FileInfo {
	var files []os.FileInfo
	for _, f := range objects {
		if (f.IsDir()) {
			files = append(files, f)
		}
	}
	return files
}

func dirTree(out *bytes.Buffer, dirName string, filesInfo bool) error {
	Buffer := new (bytes.Buffer)
	err := dirPrint(Buffer, dirName, filesInfo)
	result := strings.Replace(Buffer.String(), "\n" , "", 1)
	result += "\n"
	out.WriteString(result)
	return err
}

func dirPrint(out *bytes.Buffer, dirName string, filesInfo bool) error {
	objects, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatal(err)
	}
	if(!filesInfo) {
		objects = removeFiles(objects)
	}
	for i, file := range objects {
		if (!file.IsDir()) {
			var toAppend string
			if (i != len(objects)-1) {
				toAppend = MIDDLEUNIT + file.Name()
			} else {
				toAppend = LASTUNIT + file.Name()
			}
			if (file.Size() != 0) {
				toAppend += " (" + strconv.Itoa(int(file.Size())) + "b)"
			} else {
				toAppend += " (empty)"
			}
			out.WriteString(toAppend)
		} else {
			var child string = dirName + string(os.PathSeparator) + file.Name()
			tempBuf := new(bytes.Buffer)
			dirPrint(tempBuf, child, filesInfo)
			var temp string
			if (i != len(objects)-1) {
				out.WriteString(MIDDLEUNIT + file.Name())
				temp = strings.Replace(tempBuf.String(), "\n", "\n│\t", -1)
			} else {
				out.WriteString(LASTUNIT + file.Name())
				temp = strings.Replace(tempBuf.String(), "\n", "\n\t", -1)
			}
			out.WriteString(temp)
		}
	}
	return err
}


func main() {
	out := new(bytes.Buffer)
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	fmt.Println(out.String())
	if err != nil {
		panic(err.Error())
	}
}
