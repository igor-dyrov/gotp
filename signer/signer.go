package main

import (
	"strings"
	//"sync"
	"strconv"
	"fmt"
	"sync"
	"sort"
)

type IntString struct {
	number int
	value string
}

func SingleHash(in, out chan interface{}) {
	var wg sync.WaitGroup
	for i := range in {
		input := strconv.Itoa(i.(int))
		md5Hash := DataSignerMd5(input)
		wg.Add(1)
		go func(str string, md5str string) {
			defer wg.Done()
			var str1 string
			var str2 string
			var wgIn sync.WaitGroup
			wgIn.Add(1)
			go func(){
				defer wgIn.Done()
				str1 = DataSignerCrc32(str)
			}()
			str2 = DataSignerCrc32(md5str)
			wgIn.Wait()
			out <- str1 + "~" + str2
		}(input, md5Hash)
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	var wgOut sync.WaitGroup
	for i := range in {
		input := i.(string)
		wgOut.Add(1)
		go func(str string) {
			defer wgOut.Done()
			result := make([]string, 6)
			var wgIn sync.WaitGroup
			wgIn.Add(6)
			for i := 0; i < 6; i++ {
				go func(index int) {
					defer wgIn.Done()
					result[index] = DataSignerCrc32(strconv.Itoa(index) + str)
				}(i)
			}
			wgIn.Wait()
			out <- strings.Join(result, "")
		}(input)
	}
	wgOut.Wait()
}

func CombineResults(in, out chan interface{}) {
	result := make([]string, 0)
	for i := range in {
		result = append(result, i.(string))
	}
	sort.Strings(result)
	out <- strings.Join(result, "_")
}

func ExecutePipeline(jobs ...job) {
	var wg sync.WaitGroup
	in := make(chan interface{})
	for i,f := range jobs {
		fmt.Println(i)
		out := make(chan interface{})
		wg.Add(1)
		go func(in, out chan interface{}, curJob job){
			defer func() {
				close(out)
				wg.Done()
			}()
			curJob(in, out)
		}(in, out, f)
		in = out
	}
	wg.Wait()
}

// сюда писать код