package main

import (
	"strconv"
	"sort"
)



func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(slice []int) string {
	var result string;
	for _, value := range slice {
		result += strconv.Itoa(value)
	}
	return result
}

func MergeSlices(floatSlice []float32, intSlice []int32) []int {
	var result []int
	for _, value := range floatSlice {
		result = append(result, int(value))
	}
	for _, value := range intSlice {
		result = append(result, int(value))
	}
	return result
}

func GetMapValuesSortedByKey(input map[int]string) []string {
	var keys []int
	for i := range input {
		keys = append(keys, i)
	}
	sort.Ints(keys)
	var result []string
	for _, value := range keys {
		result = append(result, input[value])
	}
	return result
}

// сюда вам надо писать функции, которых не хватает, чтобы проходили тесты в gotchas_test.go
