package main

import (
	"fmt"
	"regexp"
	"strconv"
)

type intStack struct {
	array []int
	sp int
}

func newStack() *intStack {
	stack := new(intStack)
	stack.sp = -1
	return  stack
}

func (stack *intStack) push(value int) {
	if (stack.sp == len(stack.array) - 1) {
		stack.array = append(stack.array, value)
		stack.sp++
	} else {
		stack.sp++
		stack.array[stack.sp] = value
	}
}

func (stack *intStack) pop() (int, error) {
	if (stack.sp == -1) {
		return -1, fmt.Errorf("stack is empty")
	}
	defer func() {
		stack.sp--
	}()
	return stack.array[stack.sp], nil
}

func  (stack *intStack) len() int {
	return stack.sp + 1
}

func calc(expr string) (int, error) {
	regExpr, err := regexp.Compile(`\s*`)
	if (err != nil) {
		return -1, err
	}
	stack := newStack()
	symbols := regExpr.Split(expr, -1)
	fmt.Println(symbols)
	for _, symbol := range  symbols{
		digit, err := strconv.Atoi(symbol)
		if (err == nil) {
			stack.push(digit)
		} else {
			if (stack.len() < 2 && symbol != "") {
				return -1, fmt.Errorf("too many operators")
			}
			var stackErr error
			var op1, op2 int
			switch symbol {
			case "+":
				op2, stackErr = stack.pop()
				op1, stackErr = stack.pop()
				stack.push(op1 + op2)
			case "-":
				op2, stackErr = stack.pop()
				op1, stackErr = stack.pop()
				stack.push(op1 - op2)
			case "*":
				op2, stackErr = stack.pop()
				op1, stackErr = stack.pop()
				stack.push(op1 * op2)
			case "/":
				op2, stackErr = stack.pop()
				op1, stackErr = stack.pop()
				stack.push(op1 / op2)
			}
			if (stackErr != nil) {
				return -1, err
			}
		}
	}
	result, err := stack.pop()
	if  stack.len() > 0 {
		return -1, fmt.Errorf("stack has unused operands")
	}
	return result, nil
}

func main() {
	fmt.Println(calc("1   4  +   6  2 - * "))
}

// сюда писать код
// фукция main тоже будет тут